#!/bin/python3
#
# Display and image and print where the mouse click (release left button) on the image.
# Stop  s on q pressed

import argparse

import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())


def click(event, x, y, _flags, _param):
    if event == cv2.EVENT_LBUTTONUP:
        print("{}, {}".format(x, y), flush=True)


image = cv2.imread(args["image"])
cv2.namedWindow("image")
cv2.setMouseCallback("image", click)

# looping until 'q' key
while True:
    cv2.imshow("image", image)
    key = cv2.waitKey(1) & 0xFF

    if key == ord("q"):
        break

cv2.destroyAllWindows()
